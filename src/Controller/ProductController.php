<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Category;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * @Prefix("api")
 *
 */

class ProductController extends AbstractFOSRestController
{
    /**
     * @SWG\Response(
     * 		response=200,
	 * 		description="Returns all products")
     */
    
    public function getProductsAction()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        $view = $this->view($products, 200);
        
        return $this->handleView($view);
    }
    
    public function postProductAction(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName($request->get('name'));
        $product->setDescription($request->get('description'));
        $product->setPrice($request->get('price'));
        

        $em->persist($product);

        $em->flush();

        $view = $this->view($product, 200);

        return $this->handleView($view);
    }

    public function getProductAction($slug) 
    {
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($slug);

        if(!$product) {
            throw new HttpException(404, "Product not found");
        }
        $view = $this->view($product, 200);

        return $this->handleView($view);
    }
}
