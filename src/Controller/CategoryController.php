<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\AbstractFOSRestController;

/**
 * @Prefix("api")
 */
class CategoryController extends AbstractFOSRestController
{
    public function getCategoriesAction() 
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        if(!$categories) {
            throw new HttpException(400, "Categories not found");
        }

        $view = $this->view($categories, 200);

        return $this->handleView($view);
    }
}
